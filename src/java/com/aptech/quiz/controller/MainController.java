/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aptech.quiz.controller;

import com.aptech.quiz.domain.Question;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ADMINISTRATOR14
 */
@Controller  

public class MainController {
    private static SessionFactory factory; 
    @RequestMapping("/hello")  
    public ModelAndView helloWorld() {  
        String message = "HELLO SPRING MVC HOW R U";  
        return new ModelAndView("hellopage", "message", message);  
    }  
    @RequestMapping("/question")  
    public ModelAndView question(   ) { 
        
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) { 
        
        }
        Question question = null;// = new Question();
        //creating configuration  
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            question = (Question)session.get(Question.class, 1); 
            System.out.println("question"+question);
            System.out.println("question"+question.getId());
            System.out.println("question"+question.getName());
            tx.commit();
        }catch (HibernateException e) {
        }
        
        return new ModelAndView("question", "question", question); 
    }
    @RequestMapping(value = "/questionJSON")
    @ResponseBody
    public Question questionJSON() {  
        try{
            factory = new Configuration().configure().buildSessionFactory();
        }catch (Throwable ex) { 
        
        }
        Question question = null;// = new Question();
        //creating configuration  
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            question = (Question)session.get(Question.class, 1); 
            System.out.println("question"+question);
            System.out.println("question"+question.getId());
            System.out.println("question"+question.getName());
            tx.commit();
        }catch (HibernateException e) {
        }
        String message = "HELLO SPRING MVC HOW R U";  
        return question; 
    }
}
